# Arrays vs. Lists in Perl & Other Quandaries

One of the things that perplexed me the most in Perl was dealing with its various [variable types](http://perldoc.perl.org/perlintro.html). I readily said “scalar, hash, array - got it!” However, once I started working in perl and was mixing my variable types, I quickly realized I didn’t get it. Thats when I read [this article](http://friedo.com/blog/2013/07/arrays-vs-lists-in-perl) and things started to make more sense.

In this post, I am going to try to paraphrase or ‘rewrite’ that article as a learning exercise for myself. Hopefully it provides a slightly different way to look at the same thing.

This post assumes you have a handle on perl’s [varibale types](http://perldoc.perl.org/perlintro.html)

## Lists
I think its helpful to start thinking of lists as something other than variable types. Lists are not scalars, arrays, or hashes - but they can be used to construct array and hash variable types. Lists are

* not persisted
* [ephemeral](https://www.google.com/search?q=ephemeral&oq=ephemeral&aqs=chrome..69i57j0l5.587j0j7&sourceid=chrome&es_sm=91&ie=UTF-8)
* immutable
* have no sigil
* have no name
* distribute references
* one-dementioal

## All the data that you work with in Perl are scalar in nature
Working with perls variable types, it helps to remember this: all the data that you work with in Perl are scalar in nature. Perl has 3 variable types
* Scalar - represents a single value (strings, integers, floating point numbers, etc)
* Array - ordered collection of values
* Hash - unordered basket of key:values
So technically arrays and hashes are collections of scalars. Thus, all the data that you work with in Perl are scalar in nature


## Lists don’t exist in a scalar context
I think its helpful to see what’s happening rather than just say “don’t do it.” Lets look at an example:

```language-perl
my $office_people = ('Jim', 'Dwight', 'Pam', 'Angela');
print "$office_people\n";
```
will output
```
Angela
```
This doesn’t mean that lists in scalar contexts store the last value, that’s perhaps a result but its not really what’s happening. Lists don’t exist in a scalar context. What’s happening from a process perspective is the list is assigning each item as the value of ```$office_people``` until it doesn’t there are no values left. So whats essentially happening is
```language-perl
my $office_people = ('Jim', 'Dwight', 'Pam', 'Angela');
$office_people = ('Dwight', 'Pam', 'Angela');
$office_people = ('Pam', 'Angela');
$office_people = ('Angela');
print "$office_people\n";
```


## Lists in an array context
You often see arrays constructed with a list
```language-perl
my @office_people = ('Jim', 'Dwight', 'Pam', 'Angela');
#Note the sigil @ instead of $ in the previous example
```
This is essentially the same as
```language-perl
my @office_people;
@office_people[0] = 'Jim';
@office_people[1] = 'Dwight';
@office_people[2] = 'Pam';
@office_people[3] = 'Angela';
```
### Arrays can be constructed anonymously with lists
The use of parenthesis denotes a list, square brackets are anonymous array references with a list in them. For example
```language-perl
my $ref = [ 'Jim', 'Dwight', 'Pam', 'Angela' ];
```
is the same as
```language-perl
my @office_people = ('Jim', 'Dwight', 'Pam', 'Angela');
my $ref = \@office_people;
#\ makes the array a reference
```
## Lists in a hash context
Lists can be used to create hashes, however you need to be mindful of where your key values are. for example
```language-perl
my %office_people = ('Jim', 'Dwight', 'Pam', 'Angela');
```
would create the following hash
```language-perl
{
  'Jim' => 'Dwight',
  'Pam' => 'Angela'
}
```
which doesn’t really make sense. You’d want actual key:values like the character to actor(ess) name like
```language-perl
my %office_people = ('Jim', 'John', 'Dwight', 'Rainn', 'Pam', 'Jenna', 'Angela','Angela');
```
which would create the following hash
```language-perl
{
  'Jim' => 'John',
  'Dwight' => 'Rainn',
  'Pam' => 'Jenna',
  'Angela' => 'Angela',
}
```
This is still pretty illegible so the best way to do this is to use “fat commas”

```language-perl
my %office_people = ('Jim' => 'John', 'Dwight' =>  'Rainn', 'Pam' => 'Jenna', 'Angela' => 'Angela');
```
